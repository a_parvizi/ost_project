"""
data ingestion into RDFox
/RDFox -role admin -password admin daemon port 8080
"""

import itertools
import time
from os.path import join

import pandas as pd
import requests
import py_stringmatching as sm

ENDPOINT = 'http://localhost:8080'
USER = 'admin'
PASSWORD = 'admin'


def create_repo(repo_name: str, repo_type: str = 'par-complex-nn'):
    """
    create a repository
    """

    url = '{}/datastores/{}?type={}'.format(ENDPOINT, repo_name, repo_type)
    resp = requests.post(url, auth=(USER, PASSWORD))

    if resp.status_code in [200, 201]:
        print('db creation successful')
    elif resp.status_code == 409:
        print('db already exists')
    else:
        raise Exception('couldn\'t create a database')


def ingestion(data_dir: str, data_file: str, repo_name: str):
    """
    ingesting data via REST API
    """

    # headers = {'Content-Type': 'text/turtle'}
    graph_name = data_file.rsplit('.', 1)[0]
    graph_file = add_graph_name(data_dir, data_file, graph_name)

    start_time = time.time()
    url = '{}/datastores/{}/content'.format(ENDPOINT, repo_name)

    with open(join(data_dir, graph_file), 'rb') as ttl_file:
        resp = requests.post(url, data=ttl_file, auth=(USER, PASSWORD))

        if resp.status_code in [200, 201]:
            print('uploading file {} was successful'.format(data_file))
            print(resp.content)
        else:
            raise Exception('couldn\'t create a database')

    print('it took {} seconds'.format(time.time() - start_time))


def clear_repository(repo_name: str):
    """
    removing everything from the repositorys
    """
    url = '{}/datastores/{}/content'.format(ENDPOINT, repo_name)
    resp = requests.delete(url, auth=(USER, PASSWORD))

    if resp.status_code == 204:
        print('db was cleared')
    else:
        raise Exception('couldn\'t create a database')


def add_graph_name(data_dir: str, file_name: str, graph_name: str):
    """
    adds named graph to the file
    """
    named_graph = 'http://{}.com'.format(graph_name)

    first_location = None
    with open(join(data_dir, file_name), 'r+') as opn_file:
        all_content = opn_file.readlines()

        for idx, line in enumerate(all_content):
            if line.startswith('@prefix'):
                pass
            elif first_location is None:
                first_location = idx
                break

        final_list = all_content[:first_location] + ['<{}> {{'.format(named_graph)] + \
                     all_content[first_location:] + ['}']

    final_file = '{}-graph.trig'.format(graph_name)
    if final_list:
        with open(join(data_dir, final_file), 'w') as opn_file:
            opn_file.writelines(final_list)

    return final_file


def select_query(repo_name: str, query_name: str, output_file: str):
    """
    sending a named query to the triplestore
    """

    resp = requests.get('{}/datastores/{}/sparql'.format(ENDPOINT, repo_name),
                        params={'query': query_name},
                        headers={'Accept': 'text/csv'},
                        auth=(USER, PASSWORD))
    if resp.status_code:
        # print(resp.text)
        lines = resp.text.split('\r\n')
        print(len(lines))

        with open(join(OUTPUT_DIR, output_file), 'w', newline='\n') as write_file:
            write_file.write('\n'.join(lines))
    else:
        raise Exception(resp.content)


def insert_query(repo_name: str, query_name: str):
    """
    sending a named query to the triplestore
    """

    resp = requests.post('{}/datastores/{}/sparql'.format(ENDPOINT, repo_name),
                         data={'update': query_name}, auth=(USER, PASSWORD))
    if resp.status_code == 204:
        print('insert successful')
    else:
        raise Exception(resp.content)


def add_rule(repo_name: str, rule: str):
    """
    adding rules
    """

    resp = requests.post('{}/datastores/{}/content'.format(ENDPOINT, repo_name),
                         data=rule, auth=(USER, PASSWORD))

    if resp.status_code == 200:
        print('rule was added')
    else:
        raise Exception(resp.content)


def process_similarities(file1: str, file2: str, repo_name: str):
    """
    process the files and find similar items to be inserted (only nouns)
    """
    df_1 = pd.read_csv(join(OUTPUT_DIR, file1))
    df_2 = pd.read_csv(join(OUTPUT_DIR, file2))

    print(df_1.columns)
    print(df_2.columns)

    jr = sm.Levenshtein()

    count = 0
    for a, b in itertools.product(df_1['wordForm'], df_2['can']):
        if pd.notna(a) and pd.notna(b) and (a != b):
            res = jr.get_sim_score(a, b)
            if res >= 0.9:
                s1 = df_1.loc[df_1['wordForm'] == a, 's'].item()
                s2 = df_2.loc[df_2['can'] == b, 's'].item()

                if s1 and s1:
                    insert_data = """
                    PREFIX : <http://example.com/rules/>
                    INSERT DATA
                    {{ 
                      <{}> :is_related <{}>.
                    }}
                    """.format(s1, s2)
                    insert_query(repo_name, insert_data)
                print(a, b, res)

                count += 1

        if count >= 10:
            break


if __name__ == '__main__':
    REPO_NAME = 'test-repo'
    clear_repository(REPO_NAME)
    create_repo(REPO_NAME)

    DATA_DIR = 'data'
    FILES = ['english-wordnet-2020.trig', 'wiktionary.trig']

    for FILE_NAME in FILES:
        ingestion(DATA_DIR, FILE_NAME, REPO_NAME)

    # add a reflexive rule
    symmetric_rule = '@prefix : <http://example.com/rules/> . ' \
                   '[?p, :is_related, ?c] :- [?c, :is_related, ?p] .'
    # add insertion rule
    insert_rule = """
    @prefix cyc: <http://sw.cyc.com/2006/07/27/cyc/>.
    @prefix wik: <http://texai.org/texai/>.
    @prefix lex: <http://www.w3.org/ns/lemon/ontolex#>.
    @prefix wn: <http://wordnet-rdf.princeton.edu/ontology#>.
    @prefix : <http://example.com/rules/> .
    [?x, :is_related, ?y]
    :-
    <http://wiktionary.com>(?x, a, wik:org.texai.wiktionary.domainEntity.WiktionaryEnglishWordForm),
    <http://wiktionary.com>(?x, wik:wiktionaryWordFormInflection, cyc:Noun),
    <http://wiktionary.com>(?x, wik:wiktionaryWordForm, ?wordForm),
    <http://english-wordnet-2020.com>(?y, a, lex:LexicalEntry),
    <http://english-wordnet-2020.com>(?y, wn:partOfSpeech, wn:noun),
    <http://english-wordnet-2020.com>(?y, lex:canonicalForm, ?cf),
    <http://english-wordnet-2020.com>(?cf, lex:writtenRep, ?can),
    FILTER(LCASE(STR(?wordForm)) = LCASE(STR(?can))) .
    """
    start_time = time.time()
    rules = [symmetric_rule, insert_rule]
    for rule in rules:
        add_rule(REPO_NAME, rule)
    print('finished selection of new facts {}'.format(time.time() - start_time))

    OUTPUT_DIR = 'output'
    # finding which nodes we could apply this new is_related relation to
    QUERY_WIK = """
    prefix cyc: <http://sw.cyc.com/2006/07/27/cyc/>
    prefix wik: <http://texai.org/texai/>
    select distinct * where { 
        graph <http://wiktionary.com> {
            ?s a wik:org.texai.wiktionary.domainEntity.WiktionaryEnglishWordForm ;
               wik:wiktionaryWordFormInflection cyc:Noun;
               wik:wiktionaryWordForm ?wordForm;
        } 
    }
    """
    QUERY_WN = """
    prefix lex: <http://www.w3.org/ns/lemon/ontolex#>
    prefix wn: <http://wordnet-rdf.princeton.edu/ontology#>
    select distinct * where { 
        graph <http://english-wordnet-2020.com> {
            ?s a lex:LexicalEntry;
                wn:partOfSpeech wn:noun;
                lex:canonicalForm [ lex:writtenRep  ?can ].
        } 
    } 
    """

    # rule based approach works way faster than the direct insert
    QUERY_INSERT = """
    prefix cyc: <http://sw.cyc.com/2006/07/27/cyc/>
    prefix wik: <http://texai.org/texai/>
    prefix lex: <http://www.w3.org/ns/lemon/ontolex#>
    prefix wn: <http://wordnet-rdf.princeton.edu/ontology#>
    prefix rule: <http://example.com/rules/> 
    insert { ?x rule:is_related ?y}
    # select distinct ?x ?y
    where {
        {
            select distinct * where { 
                graph <http://wiktionary.com> {
                    ?x a wik:org.texai.wiktionary.domainEntity.WiktionaryEnglishWordForm ;
                       wik:wiktionaryWordFormInflection cyc:Noun;
                       wik:wiktionaryWordForm ?wordForm;
                } 
            } limit 1000
        }
        graph <http://english-wordnet-2020.com> {
            ?y a lex:LexicalEntry;
                wn:partOfSpeech wn:noun;
                lex:canonicalForm [ lex:writtenRep  ?can ].
        } 
        
        filter (lcase(str(?wordForm)) = lcase(str(?can)))
    }
    
    """

    WIKI_FILE = 'wiki.csv'
    WN_FILE = 'wordnet.csv'
    select_query(REPO_NAME, QUERY_WIK, WIKI_FILE)
    select_query(REPO_NAME, QUERY_WN, WN_FILE)
    process_similarities(WIKI_FILE, WN_FILE, REPO_NAME)

    # insert_query(REPO_NAME, QUERY_INSERT)
