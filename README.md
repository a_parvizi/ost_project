# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##


Start RDFox, e.g. /RDFox -role admin -password admin daemon port 8080


* Version 0.1
* Data:
    * English WordNet https://en-word.net (riot --output=ttl --formatted=trig english-wordnet-2020_orig.ttl > english-wordnet-2020.trig)
    * Wiktionary https://sourceforge.net/projects/texai/files/wiktionary-rdf/1.1/ (this needs to be converted into another format e.g. `trig` use Apache riot `riot --output=ttl --formatted=trig wiktionary.rdf > wiktionary.trig`) 

## Summary ##

The aim of this project is to merge wordnet and wiktionary datasets. These both contain headwords of a dictionary and a part of speech.

The following steps have been taken:

* Both of these datasets have been imported to named graphs. As there is no direct way to import data into named graphs, the files had to be converted in `trig`, opened, and named graph details added.
* a relation `is_related` has been introduced as a symmetric relations via a rule `[?p, :is_related, ?c] :- [?c, :is_related, ?p]`
* This relation could be added by two approaches:
    * Finding the exact match, e.g. `milk` (noun) to `milk` (noun)
    * Finding close matches, e.g `encyclopedia` and `encyclopaedia`
    

### Exact match ###
For this matching two approaches were tested:
* Via a SPARQL insert: trying to filter all the possible nouns in one dataset and match them with nouns in another dataset, while the written form of both resources (nodes) are exactly the same.
```
prefix cyc: <http://sw.cyc.com/2006/07/27/cyc/>
    prefix wik: <http://texai.org/texai/>
    prefix lex: <http://www.w3.org/ns/lemon/ontolex#>
    prefix wn: <http://wordnet-rdf.princeton.edu/ontology#>
    prefix rule: <http://example.com/rules/> 
    insert { ?x rule:is_related ?y}
    # select distinct ?x ?y
    where {
        {
            select distinct * where { 
                graph <http://wiktionary.com> {
                    ?x a wik:org.texai.wiktionary.domainEntity.WiktionaryEnglishWordForm ;
                       wik:wiktionaryWordFormInflection cyc:Noun;
                       wik:wiktionaryWordForm ?wordForm;
                } 
            } limit 1000
        }
        graph <http://english-wordnet-2020.com> {
            ?y a lex:LexicalEntry;
                wn:partOfSpeech wn:noun;
                lex:canonicalForm [ lex:writtenRep  ?can ].
        } 
        
        filter (lcase(str(?wordForm)) = lcase(str(?can)))
    }
    
```

This query was slow and was only tested for 100 matches (~29s) and a 1000 matches (~225s).

* Via datalog rules: The following query was converted into a rule and added.
```
@prefix cyc: <http://sw.cyc.com/2006/07/27/cyc/>.
    @prefix wik: <http://texai.org/texai/>.
    @prefix lex: <http://www.w3.org/ns/lemon/ontolex#>.
    @prefix wn: <http://wordnet-rdf.princeton.edu/ontology#>.
    @prefix : <http://example.com/rules/> .
    [?x, :is_related, ?y]
    :-
    <http://wiktionary.com>(?x, a, wik:org.texai.wiktionary.domainEntity.WiktionaryEnglishWordForm),
    <http://wiktionary.com>(?x, wik:wiktionaryWordFormInflection, cyc:Noun),
    <http://wiktionary.com>(?x, wik:wiktionaryWordForm, ?wordForm),
    <http://english-wordnet-2020.com>(?y, a, lex:LexicalEntry),
    <http://english-wordnet-2020.com>(?y, wn:partOfSpeech, wn:noun),
    <http://english-wordnet-2020.com>(?y, lex:canonicalForm, ?cf),
    <http://english-wordnet-2020.com>(?cf, lex:writtenRep, ?can),
    FILTER(LCASE(STR(?wordForm)) = LCASE(STR(?can))) .
```

This approach was also slow, and was abandoned.

### Close matches ###
In this approach every node in each named graph alongside the written form was extracted. A Levenshtein distance between each written forms from both graphs was calculated and anything above `0.9` was inserted via SPARQL into the default graph.

For example, the following written forms were accepted:
* encyclopedia encyclopaedia
* running shoes -- running shoe
* flannel cake -- flannel-cake
* regularization  -- regularisation
*kit and kaboodle -- kit and caboodle

Note: Levenshtein distance computes the minimum cost of transforming one string into the other. Transforming a string is carried out using a sequence of the following operators: delete a character, insert a character, and substitute one character for another.


```
PREFIX : <http://example.com/rules/>
INSERT DATA
{{ 
  <node_1> :is_related <node_2>.
}}
```

